package com.ub.backend.base.utils;

public class LangUtils {
    public static String invertLang(String lang){
        switch (lang){
            case "ru" :return "en";
            case "en" :return "ru";
            default:return "en";
        }
    }
}
