$(function () {

    /*$('table.js-table-float-thead').floatThead({
        position: 'fixed',
        scrollingTop: 65
    });
    $('table.js-table-float-thead thead').css('background', '#ECEFF1');*/

    $('.js-ckeditor').ckeditor();

    $('.js-dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove':  'Remove',
            'error':   'Ooops, something wrong happended.'
        }
    });

    $('.js-link-warning-action').click(function () {
        var $this = $(this);
        var href = $this.prop('href');
        swal({
                title: "Are you sure?",
                text: $this.data("text"),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No",
                closeOnConfirm: false
            },
            function () {
                location.href = href.toString();
            }
        );

        return false;
    });
    $('.js-link-remove-doc').click(function () {
        var href = $(this).prop('href');
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this document!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No",
                closeOnConfirm: false
            },
            function () {
                location.href = href.toString();
            }
        );

        return false;
    });
});