<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--@elvariable id="search" type="com.ub.core.user.view.search.SearchUserResponse"--%>

<table class="responsive-table bordered highlight">
    <thead>
    <tr>
        <th>Email</th>
        <th>Login</th>
        <th>Телефон</th>
        <th>Имя</th>
        <th>Статус</th>
        <th>Роли</th>
    </tr>
    </thead>
    <tbody class="js-modal-table-body">
    <c:forEach items="${search.result}" var="doc" varStatus="status">
        <tr class="js-modal-table-item" data-id="${doc.id}" data-title="${doc.email}">
            <td>${doc.noNullEmail}</td>
            <td>${doc.login}</td>
            <td>${doc.phoneNumber}</td>
            <td>${doc.name}</td>
            <td>${doc.status.title}</td>
            <td>
                <c:forEach items="${doc.roles}" var="role">
                    ${role.title};<br/>
                </c:forEach>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="section center-align">
    <ul class="pagination">
        <c:if test="${search.prevNum ne search.currentPage}">
            <li class="${search.prevNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a class="js-modal-pagination" data-page="${search.prevNum}" data-query="${search.query}">
                    <i class="mdi-navigation-chevron-left"></i>
                </a>
            </li>
        </c:if>

        <c:forEach items="${search.pagination}" var="page">
            <li class="${search.currentPage eq page ? 'active' : ''}">
                <a class="js-modal-pagination" data-page="${search.currentPage ne page ? page : ''}">${page + 1}</a>
            </li>
        </c:forEach>

        <c:if test="${search.nextNum ne search.currentPage}">
            <li class="${search.nextNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a class="js-modal-pagination" data-page="${search.nextNum}" data-query="${search.query}">
                    <i class="mdi-navigation-chevron-right"></i>
                </a>
            </li>
        </c:if>
    </ul>
</div>