<%@ page import="com.ub.backend.article.routes.ArticleClientRoute" %>
<section>
    <div class="container header">
        <div class="row">
                <div class="col-md-6">
                    <div class="header-logo">
                        <img src="static/backend/img/logo.jpg" class="header-logo-img"/>
                    </div>

                    <div class="header-title">
                        Статьи, 4.
                    </div>
                </div>
            <div class="col-md-6">
                <div class="header-article-new">
                    <a href="/article/add" class="header-article-new-link">
                        <img class="header-article-new-link-img" src="static/backend/img/add.jpg"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article">
        <div class="row">

            <div class="col-md-6">
                <div class="article-list-item">
                    <a href="<%=ArticleClientRoute.VIEW%>" class="article-list-item-link">
                        <div class="article-list-item-pic" style="background-image: url('/static/backend/img/Snowstorm.jpg')"></div>
                        <div class="article-list-item-title">
                            Hopeful Thoughts Reloaded (2017 Edit)
                        </div>
                        <div class="article-list-item-date">
                            18 окт, 2017
                        </div>
                        <div class="article-list-item-preview">
                            A re-work of 2015's remix of my 2013's famous track.
                        </div>
                        <div class="article-list-item-audio">
                            <audio src="/static/backend/audios/Packster%20-%20Snowstorm.mp3" controls preload="metadata"></audio>
                        </div>
                    </a>
                </div>

                <div class="article-list-item">
                    <a href="<%=ArticleClientRoute.VIEW%>" class="article-list-item-link">
                        <div class="article-list-item-pic" style="background-image: url('/static/backend/img/Snowstorm.jpg')"></div>
                        <div class="article-list-item-title">
                            Snowstorm
                        </div>
                        <div class="article-list-item-date">
                            20 янв, 2018
                        </div>
                        <div class="article-list-item-preview">
                            A new release!
                        </div>
                        <div class="article-list-item-audio">
                            <audio src="/static/backend/audios/Packster%20-%20Snowstorm.mp3" controls preload="metadata"></audio>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="article-list-item">
                    <a href="<%=ArticleClientRoute.VIEW%>" class="article-list-item-link">
                        <div class="article-list-item-pic" style="background-image: url('/static/backend/img/Snowstorm.jpg')"></div>
                        <div class="article-list-item-title">
                            TVD-property for linear differential schemes
                        </div>
                        <div class="article-list-item-date">
                            20 янв, 2018
                        </div>
                        <div class="article-list-item-preview">
                            Monotone schemes are attractive for solving engineering and scientific problems because they do not produce non-physical solutions.
                        </div>
                        <div class="article-list-item-audio">
                            <audio src="/static/backend/audios/Packster%20-%20Snowstorm.mp3" controls preload="metadata"></audio>
                        </div>
                    </a>
                </div>

                <div class="article-list-item">
                    <a href="<%=ArticleClientRoute.VIEW%>" class="article-list-item-link">
                        <div class="article-list-item-pic" style="background-image: url('/static/backend/img/Snowstorm.jpg')"></div>
                        <div class="article-list-item-title">
                            Dysis - Zetnai
                        </div>
                        <div class="article-list-item-date">
                            10 май, 2017
                        </div>
                        <div class="article-list-item-preview">
                            Nice release from my Spanish friend!
                        </div>
                        <div class="article-list-item-audio">
                            <audio src="/static/backend/audios/Packster%20-%20Snowstorm.mp3" controls preload="metadata"></audio>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>