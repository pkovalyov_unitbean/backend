<session>
    <div class="container article-done">
        <div class="row">
            <div class="col-md-12">
                <a href="/">
                    <img src="/static/backend/img/success.png" class="article-done-img">
                </a>
                <div class="article-done-text">
                    Вы пришли к успеху!
                </div>
            </div>
        </div>
    </div>
</session>